const express = require('express');
const cors = require('cors');
const mysql = require('mysql');

const drivers = require('./app/drivers');
const officeWorkers = require('./app/officeWorkers');
const cars = require('./app/cars');
const passports = require('./app/passports');
const driversLicenses = require('./app/driversLicenses');
const orders = require('./app/orders');

const app = express();
const port = 8000;

app.use(cors());
app.use(express.json());

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '0777253269s',
    database: 'taxi_service'
});

connection.connect((err) => {
    if (err) throw err;

    app.use('/drivers', drivers(connection));
    app.use('/officeworkers', officeWorkers(connection));
    app.use('/cars', cars(connection));
    app.use('/passports', passports(connection));
    app.use('/driverslicenses', driversLicenses(connection));
    app.use('/orders', orders(connection));

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});