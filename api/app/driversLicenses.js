const express = require('express');

const router = express.Router();

const createRouter = (db) => {
    router.get('/', (req, res) => {
        db.query('SELECT * FROM `DriversLicense`', function (error, results, fields) {
            if (error) throw error;

            console.log('RESULTS:______', results);
            console.log('FIELDS:_______', fields);

            res.send(results);
        });
    });

    return router;
};

module.exports = createRouter;